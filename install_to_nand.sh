#!/bin/sh

if [[ $(./check_a_file.sh rootfs.tar.gz) != 0 ]]; then
    echo "rootfs.tar.gz not found"
    exit
fi

if [ -z "$1" ]; then
    echo "No argument supplied"
    exit
fi
DEV=$1

par1=${DEV}1
par2=${DEV}2
if [ ! -b $par1 ]; then
        par1=${DEV}p1
        par2=${DEV}p2
fi

if [ ! -b $par1 ]; then
    echo "format necessary"
    exit
fi

# Bu alan da sadece vfat bölümün dosya yüklemesi yapılır. ext bölümüne dosya yüklenmez
TARGET="tgt_vfat"

#clean all old sub folder.
rm *.img
rm -rf $TARGET

mkdir -p $TARGET/sd_boot/script


cp spl/*.SPL $TARGET/
cp -a spl/spl $TARGET/sd_boot/
cp -a u-boot $TARGET/sd_boot/
cp ush/save_to_nand.ush $TARGET/sd_boot/script/u-bootscript.ush

./create_fw_img.sh ubi_kernel
cp -a ubifs_fw $TARGET/sd_boot/

./create_fs_img.sh rootfs.tar.gz
cp -a ubifs_rootfs $TARGET/sd_boot/

mkdir tmp_mnt
options="rw,nosuid,nodev,uid=1000,gid=1000,shortname=mixed,dmask=0077,utf8=1,showexec,flush,uhelper=udisks2"
sudo mkfs.vfat $par1
sudo mount -o "$options" $par1 tmp_mnt
cp -a $TARGET/* tmp_mnt/
echo "syncing"
sync
sudo umount tmp_mnt
./clean.sh
