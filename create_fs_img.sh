#!/bin/sh

if [ -z "$1" ]
  then
    echo "No argument supplied"
    exit
fi

TARGET="ubifs_rootfs"
rm -rf $TARGET

FILE=$1

if grep -q "tar.gz" "$FILE"; then
    mkdir -p tmp_fs;
    sudo tar -xf $FILE -C tmp_fs/
    FILE="tmp_fs"
fi

ARGS="-v -m 2048 -e 126976 -c 1024"
OUTPUT_FS="ubifs_rootfs.img"

mkdir -p $TARGET
sudo mkfs.ubifs $ARGS -o $OUTPUT_FS -r $FILE/
mv $OUTPUT_FS $TARGET/
