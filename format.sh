#!/bin/sh

if [ -z "$1" ]
  then
    echo "No argument supplied"
    exit
fi

DEV=$1
sudo dd if=xarina_m11.mbr of=$DEV
