#!/bin/sh

if [ -z "$1" ]
  then
    echo "No argument supplied"
    exit
fi

TARGET="ubifs_fw"
rm -rf $TARGET

FILE=$1

ARGS="-v -m 2048 -e 126976 -c 88"

OUTPUT_FW="ubifs_fw.img"

mkdir -p $TARGET
mkfs.ubifs $ARGS -o $OUTPUT_FW -r $FILE/
mv $OUTPUT_FW $TARGET/
